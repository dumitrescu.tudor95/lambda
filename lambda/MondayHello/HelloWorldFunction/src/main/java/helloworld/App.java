package helloworld;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;

/**
 * Handler for requests to Lambda function.
 */
public class App implements RequestHandler<String, String> {

    public App(){}

    public String handleRequest(final String input, final Context context) {
        try {
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            ResultSet set = statement.executeQuery("SELECT * FROM users");
            return set.toString();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }



    private Connection getConnection() throws SQLException {

        String url = "hope-dev-database.c6113o9pvyrn.eu-central-1.rds.amazonaws.com:5432";
        String username = "postgres";
        String password = "jnvbIuVUi5P0d88CfZVJ";
        return DriverManager.getConnection(url, username, password);
    }


}
